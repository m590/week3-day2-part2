const mongoose = require('mongoose');
const Game = mongoose.model('Game');

module.exports.gamesGetAll = function (req, res) {
    let count = 10;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 25;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (isNaN(count) || isNaN(offset)) {
        res.status(400).json({content: 'Query parameter count and offset should be numbers.'});
        return;
    }

    Game.find().skip(offset).limit(count).exec(function (err, games) {
        const response = {
            status: 200,
            content: games
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else {
            res.status(response.status).json(response.content);
        }
    });

}

function _successGetOne(res, game) {
    const response = {
        status: 200,
        content: game
    };
    if (!game) {
        response.status = 400;
        response.content = {content: 'Game not found'};
    }
    res.status(response.status).json(response.content);
}

function _failedGetOne(res, err) {
    console.log('game.ctrl.get.game', err);
    res.status(500).json({content: 'Internal error'});
}

module.exports.gamesGetOne = function (req, res) {
    Game.findById(req.params.gameId).exec()
        .then(_successGetOne.bind(null, res))
        .catch(_failedGetOne.bind(null, res));
}

function _successAddOne(res, game) {
    res.status(201).json(game);
}

function _failedAddOne(res, err) {
    console.log('game.ctrl.add.game', err);
    res.status(500).json({content: 'Internal error'});
}

module.exports.gamesAddOne = function (req, res) {
    console.log("Add One games");

    const newGame = {
        title: req.body.title,
        year: parseInt(req.body.year),
        price: parseFloat(req.body.price),
        minPlayers: parseInt(req.body.minPlayers),
        maxPlayers: parseInt(req.body.maxPlayers),
        minAge: parseInt(req.body.minAge),
        designers: [],
        publisher: {}
    };
    if (req.body.rate) {
        newGame.rate = parseInt(req.body.rate);
    }
    console.log('newGame', newGame);

    Game.create(newGame)
        .then(_successAddOne.bind(null, res))
        .catch(_failedAddOne.bind(null, res));
}

function _successSaveGame(res, game) {
    res.status(200).json(game);
}

function _failedSaveGame(res, err) {
    console.log('game.ctrl.save.game', err);
    res.status(response.status).json(response.content);
}

const _fullUpdateGame = function (req, res, game) {
    game.title = req.body.title;
    game.year = parseInt(req.body.year);
    game.rate = parseInt(req.body.rate);
    game.price = parseFloat(req.body.price);
    game.minPlayers = parseInt(req.body.minPlayers);
    game.maxPlayers = parseInt(req.body.maxPlayers);
    game.minAge = parseInt(req.body.minAge);

    game.save()
        .then(_successSaveGame.bind(null, res))
        .catch(_failedSaveGame.bind(null, res));
}

function _successUpdateGame(req, res, game) {
    if (game) {
        _fullUpdateGame(req, res, game);
    } else {
        console.log('game.ctrl.update.game not found');
        res.status(404).json('GamFull update one gamee not found');
    }
}

function _failureUpdateGame(res, err) {
    console.log('game.ctrl.update.game err', err);
    res.status(500).json('Internal error');
}

module.exports.gamesFullUpdateOne = function (req, res) {
    console.log("Full update one game");
    Game.findById(req.params.gameId).exec()
        .then(_successUpdateGame.bind(null, req, res))
        .catch(_failureUpdateGame.bind(null, res));

}

const _partialUpdateGame = function (req, res, game) {
    if (req.body.title) {
        game.title = req.body.title;
    }
    if (req.body.year) {
        game.year = parseInt(req.body.year);
    }
    if (req.body.rate) {
        game.rate = parseInt(req.body.rate);
    }
    if (req.body.price) {
        game.price = parseFloat(req.body.price);
    }
    if (req.body.minPlayers) {
        game.minPlayers = parseInt(req.body.minPlayers);
    }
    if (req.body.maxPlayers) {
        game.maxPlayers = parseInt(req.body.maxPlayers);
    }
    if (req.body.minAge) {
        game.minAge = parseInt(req.body.minAge);
    }

    game.save()
        .then(_successSaveGame.bind(null, res))
        .catch(_failedSaveGame.bind(null, res));
}

function _successPartialUpdateGame(res, game) {
    if (game) {
        _partialUpdateGame(req, res, game);
    } else {
        console.log('game.ctrl.update.game not found');
        res.status(404).json('Game not found');
    }
}

module.exports.gamesPartialUpdateOne = function (req, res) {
    console.log("Partial update one game");

    Game.findById(req.params.gameId).exec()
        .then(_successPartialUpdateGame.bind(null, res))
        .catch(_failureUpdateGame.bind(null, res));
}

module.exports.gamesDeleteOne = function (req, res) {
    console.log("Delete one game");
    Game.findByIdAndDelete(req.params.gameId).exec()
        .then(_successGetOne.bind(null, res))
        .catch(_failedGetOne.bind(null, res));
}