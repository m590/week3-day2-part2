const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = mongoose.model("User");

function _successCreateUser(res, savedUser) {
    res.status(201).json({message: "Created"});
}

function _failureCreateUser(res, err) {
    console.log('UserCtrl.register couldn\'t save user', err);
    res.status(500).json({message: "Internal error"});
}

function _successHash(req, res, hashPassword) {
    const newUser = {
        name: req.body.name,
        username: req.body.username,
        password: hashPassword
    }
    User.create(newUser)
        .then(_successCreateUser.bind(null, res))
        .catch(_failureCreateUser.bind(null, res));
    ;
}

function _failureHash(res, err) {
    console.log('UserCtrl.register create hash error', err);
    res.status(500).json({message: "Internal error"});
}

function _successSalt(req, res, password, salt) {
    bcrypt.hash(password, salt)
        .then(_successHash.bind(null, req, res))
        .catch(_failureHash.bind(null, res));
}

function _failureSalt(res, err) {
    console.log('UserCtrl.register create salt error', err);
    res.status(500).json({message: "Internal error"});
}

module.exports.register = function (req, res) {

    if (!req.body.name || !req.body.username || !req.body.password) {
        res.status(400).json({message: "name, username, password fields are required"});
        return;
    }

    bcrypt.genSalt(10, 0)
        .then(_successSalt.bind(null, req, res, req.body.password))
        .catch(_failureSalt.bind(null, res));
}

function _successComparePass(res, foundUser, same) {
    const token = jwt.sign({name: foundUser.name}, process.env.pass_phrase, {expiresIn: 3600});
    res.status(200).json({success: true, token: token});
}

function _failureComparePass(res, err) {
    console.log('UserCtrl.login comparing password error', err);
    res.status(400).json({message: "Wrong credential"});
}

function _successFoundUser(req, res, foundUser) {
    bcrypt.compare(req.body.password, foundUser.password)
        .then(_successComparePass.bind(null, res, foundUser))
        .catch(_failureComparePass.bind(null, res));
}

function _failureFoundUser(res, err) {
    console.log('UserCtrl.login user not found', err);
    res.status(500).json({message: "Internal error"});
}

module.exports.login = function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.status(400).json({message: "username, password fields are required"});
        return;
    }

    const query = {
        username: req.body.username
    }
    User.findOne(query).exec()
        .then(_successFoundUser.bind(null, req, res))
        .catch(_failureFoundUser.bind(null, res));
    ;
}

module.exports.authenticate = function (req, res, next) {
    const headerAuth = req.headers.authorization;
    if (headerAuth) {
        const token = headerAuth.split(' ')[1];
        jwt.verify(token, process.env.pass_phrase, function (err, decoded) {
            if (decoded) {
                next();
            } else {
                if (err) {
                    console.log('user.ctrl authenticate error', err);
                }
                res.status(401).json({message: 'Unauthorized'});
            }
        });
    } else {
        res.status(401).json({message: 'Unauthorized'});
    }
}