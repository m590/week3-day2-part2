const mongoose = require('mongoose');
const Game = mongoose.model('Game');

function _successGetOne(res, game) {
    const response = {};
    if (game) {
        response.status = 200;
        response.content = game.publisher;
    } else {
        response.status = 400;
        response.content = {content: 'Game not found'};
    }
    res.status(response.status).json(response.content);
}

function _failedGetOne(res, err) {
    console.log('publisher.ctrl.get.one', err);
    res.status(500).json({content: 'Internal error'});
}

module.exports.publisherGetOne = function (req, res) {
    Game.findById(req.params.gameId).select("publisher").exec()
        .then(_successGetOne.bind(null, res))
        .catch(_failedGetOne.bind(null, res));
}

function _successSaveGame(res, game) {
    res.status(200).json(game);
}

function _failedSaveGame(res, err) {
    console.log('publisher.ctrl.save.game', err);
    res.status(response.status).json(response.content);
}

const _addPublisher = function (req, res, game) {
    game.publisher.name = req.body.name;
    game.publisher.location.type = "Point";
    game.publisher.location.coordinates = [parseFloat(req.body.lng), parseFloat(req.body.lat)];

    game.save()
        .then(_successSaveGame.bind(null, res))
        .catch(_failedSaveGame.bind(null, res));
}

function _successAddPublisher(res, game) {
    if (game) {
        _addPublisher(req, res, game);
    } else {
        console.log('publisher.ctrl.addpublisher.game not found');
        res.status(404).json('Game not found');
    }
}

function _failureAddPublisher(res) {
    console.log('publisher.ctrl.addpublisher.game err', err);
    res.status(500).json('Internal error');
}

module.exports.publisherAddOne = function (req, res) {
    Game.findById(req.params.gameId).exec()
        .then(_successAddPublisher.bind(null, res))
        .catch(_failureAddPublisher.bind(null, res));
}
