const express = require('express');
const gameCtrl = require('./../controller/games.controller.js');
const publisherCtrl = require('./../controller/publisher.ctrl');
const userCtrl = require('./../controller/user.ctrl');

const router = express.Router();

router.route("/games")
    .get(gameCtrl.gamesGetAll)
    .post(userCtrl.authenticate, gameCtrl.gamesAddOne);

router.route("/games/:gameId")
    .get(gameCtrl.gamesGetOne)
    .put(userCtrl.authenticate, gameCtrl.gamesFullUpdateOne)
    .patch(userCtrl.authenticate, gameCtrl.gamesPartialUpdateOne)
    .delete(userCtrl.authenticate, gameCtrl.gamesDeleteOne);

router.route("/games/:gameId/publisher")
    .get(publisherCtrl.publisherGetOne)
    .post(userCtrl.authenticate, publisherCtrl.publisherAddOne);

// router module - g export hiine
module.exports = router;