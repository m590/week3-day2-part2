const express = require('express');
const path = require('path');
require('dotenv').config();
require('./api/data/db');

const router = require('./api/routes');
const userRouter = require('./api/routes/user.router');

const app = express();
app.use(function (req, res, next) {
    next();
});

app.use("/node_modules", express.static(path.join(__dirname, "node_modules")));
app.use(express.static(path.join(__dirname, process.env.PUBLIC_FOLDER)));

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use("/api", router);
app.use("/api/user", userRouter);


const server = app.listen(process.env.port, function () {
    console.log('Mean Games server started at', server.address().port);
});