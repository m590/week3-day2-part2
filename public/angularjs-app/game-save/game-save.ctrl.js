meanGames.controller("GameSaveController", GameSaveController);

function GameSaveController(GameService, $location) {
    const vm = this;
    vm.newGame = {};
    vm.gameId = $location.$$search.id;

    function _successGame(result){
        vm.newGame = result;
        vm.newGame.hits = vm.newGame.hits.join(',');
    }

    if (vm.gameId) {
        vm.title = 'Update a game';
        GameService.getGame(vm.gameId).then(_successGame);
    } else {
        vm.title = 'Add a game';
    }

    vm.saveGame = function () {
        if (vm.newGameForm.$valid) {
            if (vm.gameId) {
                GameService.updateGame(vm.newGame).then(function (result) {
                    $location.url('/');
                });
            } else {
                GameService.addGame(vm.newGame).then(function (result) {
                    $location.url('/');
                });
            }
        }
    }
}