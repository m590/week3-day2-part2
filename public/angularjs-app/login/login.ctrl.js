angular.module('meanGames').controller("LoginController", LoginController);

function LoginController(UserFactory, $window, $location, jwtHelper, AuthFactory) {
    const vm = this;
    vm.isLoggedIn = function () {
        if (AuthFactory.authenticated) {
            return true;
        } else {
            return false;
        }
    }

    function _successLogged(response) {
        if (response.success) {
            vm.err = '';
            $window.sessionStorage.token = response.token;
            const token = $window.sessionStorage.token;
            const decodedToken = jwtHelper.decodeToken(token);
            vm.loggedInUser = decodedToken.name;
            AuthFactory.authenticated = true;
            vm.username = '';
            vm.password = '';
            $location.url('/');
        } else {
            vm.err = response;
        }
    }

    vm.login = function () {
        UserFactory.login({username: vm.username, password: vm.password})
            .then(_successLogged)
            .catch(function (err) {
                vm.err = err;
            });
    }

    vm.logout = function () {
        AuthFactory.authenticated = false;
        delete $window.sessionStorage.token;
        $location.url('/');
    }

    vm.isActiveTab = function (url) {
        var currentPath = $location.path().split("/")[1];
        return (url === currentPath ? "active" : "");
    }
}