meanGames.controller("GamesController", GamesController);

function GamesController(GameService, $location, AuthFactory) {
    const vm = this;
    vm.name = "tomo";

    vm.loadGames = function(){
        GameService.getAllGames().then(function (res) {
            vm.games = res;
        });
    }

    vm.toAdd = function () {
        $location.url("/savegame");
    }

    vm.toUpdate = function (gameId) {
        $location.url("/savegame?id=" + gameId);
    }

    vm.delete = function (gameId) {
        GameService.deleteGame(gameId).then(function (result) {
            vm.loadGames();
        });
    }

    vm.isLoggedIn = function () {
        if (AuthFactory.authenticated) {
            return true;
        } else {
            return false;
        }
    }
}