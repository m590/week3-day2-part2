angular.module('meanGames').controller('RegisterController', RegisterController);

function RegisterController(UserFactory) {
    const vm = this;

    vm.user = {};

    function _successRegister(response) {
        if (response.status == 200) {
            vm.err = '';
            vm.message = "Success";
        } else {
            vm.err = response.data;
        }
    }

    vm.register = function () {
        if (vm.user.password !== vm.user.repeatPassword) {
            console.log('register');
            vm.err = "Password must match";
        } else {
            UserFactory.register(vm.user)
                .then(_successRegister)
                .catch(function (err) {
                    vm.err = err;
                });
        }
    }
}