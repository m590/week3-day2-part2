meanGames.controller("GameController", GameController);

function _getStarsArray(rate) {
    return new Array(rate);
}

function GameController(GameService, $routeParams) {
    const vm = this;

    const gameId = $routeParams.id;

    function _successGameFound(res) {
        vm.game = res;
        vm.rating = _getStarsArray(vm.game.rate);
    }

    GameService.getGame(gameId).then(_successGameFound);

}