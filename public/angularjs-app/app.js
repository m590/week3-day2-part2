var meanGames = angular.module("meanGames", ['ngRoute', 'angular-jwt'])
    .config(config);

function config($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('AuthenticationInterceptor');
    $routeProvider.when("/", {
        templateUrl: "angularjs-app/welcome/welcome.html",
    }).when("/games", {
        templateUrl: "angularjs-app/game-list/games.html",
        controller: "GamesController",
        controllerAs: "vm"
    }).when("/game/:id", {
        templateUrl: "angularjs-app/game-display/game-display.html",
        controller: "GameController",
        controllerAs: "vm"
    }).when("/savegame", {
        templateUrl: "angularjs-app/game-save/game-save.html",
        controller: "GameSaveController",
        controllerAs: "vm"
    }).when("/register", {
        templateUrl: "angularjs-app/register/register.html",
        controller: "RegisterController",
        controllerAs: "vm",
        access: {restrict: false}
    }).when("/profile", {
        templateUrl: "angularjs-app/profile/profile.html",
        access: {restrict: true}
    });
}